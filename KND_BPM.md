## Описание структуры реализации BPM в проекте KND
### Модели процессов

Стандартный экран управления моделями процессов `ProcModelBrowse` переопределен классом `ExtProcModelBrowse`
>com.haulmont.kndofficework.web.screens.ExtProcModelBrowse

В классе переопределен метод `deploy()` - развертывание модели процесса.
> подробно о развертывании модели процесса:
> [можно прочитать здесь](https://doc.cuba-platform.com/bpm-6.10-ru/qs_process_model_deployment.html)

По сравнению со стандартным кодом добавлен следующий код:
```java
ProcDefinition pd = processRepositoryService.deployProcessFromXml(processXml, procDefinition, procModelsDs.getItem());  
if (pd != null) {  
    showNotification(pd.getActId() + "-" + getMessage("extProcessDeployed"), NotificationType.HUMANIZED);  
 if (!pd.getProcRoles().isEmpty()) {  
        List<ProcRole> procRolesList = pd.getProcRoles();  
  procRolesList.forEach(role -> addRolesMatch(role));  
  }  
}
private void addRolesMatch(ProcRole role) {  
    rolesMatchService.addRolesMatchForRole(role);
```
Помимо стандартной службы `ProcessRepositoryService` (подробнее о стандартных сервисах [здесь](https://doc.cuba-platform.com/bpm-6.10-ru/process_repository_service.html)) используется служба `RolesMatchService` 

> com.haulmont.kndofficework.core.service.RolesMatchServiceBean

в которой через сущность `RolesMatch` производится сопоставление процессных ролей реальным ролям пользователей в системе (`таблица KNDOFFICEWORK_ROLES_MATCH`).
Для сопоставления процессных ролей системным ролям используется экран которого нет в базовой реализации: 
> com.haulmont.kndofficework.web.rolesmatch.RolesMatchBrowse

![enter image description here](https://lh3.googleusercontent.com/4SEqvvT93metZ3lJD-AIBlQNMyuF5AqqqvuJXk0VNyASGu1-9T1_QdmyzgWfuwL-z_5hfu-JijcJ=s1600)

Сама модель храниться в таблицах `act_re_model` и `act_ge_bytearray`  в формате `json`. Модель экспортируется и импортируется в формате `json`

В результате развертывания модели процесса создается объект с описаниями процесса (`ProcDefinition`).  Перечень описаний процесса доступен в меню **КНД** :
> Программные настройки -> Бизнес процессы -> Определения процесса

Этот экран (`ExtProcDefinitionBrowse`) так же переопределен по сравнению со стандартным. Переопределен метод `doRemove` который при удалении объекта с описанием процесса производит так же удаление записи о сопоставлении процессных ролей реальным ролям пользователей в системе.

### Динамическое формирование базовых фреймов модели КНД

Рассмотрим реализацию динамического формирования фреймов в проекте **КНД** на примере создания окна плана проверок . 
> КНМ -> Планирование  КНМ

Изначально реализуется главный фрейм `CheckPlanCombined extends AbstractWindow`, который содержит основной источник данных `checkPlansDs` и вложенные коллекции сущностей двух типов `Check` (*Плановые проверки, Внеплановые проверки, Профилактические мероприятия*) и `Raid`(Рейды). Кроме того  xml-описание экрана содержит базовое форматирование где определена начальная таблица с данными источника `checkPlansDs` и два динамических фрейма:
```xml
<vbox id="eventListBox" height="100%"/>
```
```xml
<vbox id="infoBox"/>
```
которые разграничены соответствующими элементами разметки `vbox`и `split`.
Так как в плане мероприятий содержаться сущности различных типов с различными наборами атрибутов, то для той или иной сущности генерируется свой фрейм в зависимости от того какая сущность выбрана в базовой таблице.
Возможные варианты фреймов, а так же фрейм по умолчанию прописываются в константах контролера экрана:
```java
private static final String FRAME_ID = "eventListFrame";  
private static final Map<EventTypes, String> DEFINED_EVENT_LIST_FRAMES = new HashMap<>();  
private static final String DEFAULT_FRAME = "knd$CheckList.frame";  
  
static {  
    DEFINED_EVENT_LIST_FRAMES.put(EventTypes.CHECK, DEFAULT_FRAME);  
  DEFINED_EVENT_LIST_FRAMES.put(EventTypes.PREVENTION, "knd$PreventionList.frame");  
  DEFINED_EVENT_LIST_FRAMES.put(EventTypes.RAID, "knd$RaidList.frame");  
}
```
Так же  в контролере экрана инжектируется соответствующий компонент разметки:
```java
@Inject  
private VBoxLayout eventListBox;
```
 В контролере экрана `CheckPlanCombined` в методе `Init`  определена функция `reloadEventListFrame` 
```java
 private void reloadEventListFrame(CheckPlan checkPlan) {  
    String eventListFrameId = DEFINED_EVENT_LIST_FRAMES.getOrDefault(checkPlan == null ?  
            null : checkPlan.getEventType().getBasicType(), DEFAULT_FRAME);  
  Frame frameComponent = (Frame) eventListBox.getComponent(FRAME_ID);  
 if (frameComponent != null) {  
        unregisterComponent(frameComponent);  
  eventListBox.remove(frameComponent);  
  }  
    frameComponent = openFrame(eventListBox, eventListFrameId, Collections.singletonMap("checkPlan", checkPlan));  
  frameComponent.setId(FRAME_ID);  
}
```
которая перечитывает содержимое `eventListBox` в зависимости от выбранного экземпляра плана проверок:
```java
initProcActionsPopupButton();  
if (e.getItem() != null) {  
    reloadEventListFrame(e.getItem());  
}
```
как видно из приведенного выше кода могут быть отображены следующие фреймы: (`nd$CheckList.frame`-по умолчанию, `knd$PreventionList.frame`, `knd$RaidList.frame`).  Все указанные фреймы наследуются от абстрактного класса `com.haulmont.knd.web.checkplan.frame.EventAbstractListFrame` в котором в свою очередь определен другой фрейм (другие фреймы)  по такой же схеме:
```java
protected static final String INFO_FRAME_ID = "infoFrame";
```
и ижектирован элемент базового экрана `CheckPlanCombined`
```java
@Inject  
protected VBoxLayout infoBox;
```
а конкретный  фрейм определяется  в классе наследнике путем переопределения функции `getFrameId()`
```java
@Override  
protected String getFrameId() {  
    return "knd$CheckInfo.frame";  
}
```
и дальше можно продолжать пока хватит фантазии и быстродействия сервера - в случае с планом проверок выводится фрейм с информацией по субъектам проверки - абстрактный класс `com.haulmont.knd.web.checkplan.frame.info.EventInfoAbstractFrame` :
```java
protected static final String FRAME_ID = "subjectEditFrame";  
protected static final Map<Class<? extends Subject>, String> DEFINED_SUBJECT_FRAMES = new HashMap<>();  
protected static final String DEFAULT_FRAME = "knd$Subject.LegalEntityPlan.frame";  
  
static {  
    DEFINED_SUBJECT_FRAMES.put(LegalEntity.class, DEFAULT_FRAME);  
  DEFINED_SUBJECT_FRAMES.put(Person.class, "knd$Subject.PersonPlan.frame");  
  DEFINED_SUBJECT_FRAMES.put(SelfEmployer.class, "knd$Subject.SelfEmployerPlan.frame");  
}
```
где все дальнейшие фреймы определяются по той же схеме. Кроме того в этом классе определено все что связано с отображением документов и истории проверок по конкретному субъекту.

### Динамическое формирование фрейма для работы с процессными действиями.

Для работы с процессными действиями **CUBA** использует фрейм `ProcActionsFrame`. Подробнее об этом фрейме можно прочитать  [здесь](https://doc.cuba-platform.com/bpm-6.10-ru/proc_actions_frame.html). После инициализации во фрейме автоматически отобразятся:

-   кнопка запуска процесса, если процесс не запущен;
    
-   кнопки, соответствующие выходам из задачи, если процесс запущен и текущий пользователь имеет активную задачу;
    
-   кнопка отмены процесса;
    
-   информация о задаче (имя и дата создания).

Остановимся на стадии запуска процесса подробнее. При создании модели процесса мы обязаны создать стартовое событие. В нем мы можем определить стартовую форму с которой и будет начинаться процесс:
![enter image description here](https://lh3.googleusercontent.com/y4SeJEx_KxJscrV-b_ccNOAg4E6A9niAzFqvL915V76pfWYhYbMDRb3cfMB5ufOxtiGU-PaHqJXr=s1200)
Изначально в системе зарегистрирована только одна форма `Standart form` которая однако уже содержит в себе подключаемые фреймы комментариев, отображения списка процессных ролей и приложений документов к процессу. Стартовая форма в модели процесса может быть не указана, в этом случае движок *Activity* будет использовать форму установленную по умолчанию. 
В случае **КНД** основное переопределение касается связки процессных ролей с ролями системы. Поэтому базовая стартовая форма в проекте переопределена. Для добавления кастомных форм в систему `bpm` необходимо переопределить свойство `bpm.formsConfig` в файле `web-app.properties` (подробнее [здесь](https://doc.cuba-platform.com/bpm-6.10-ru/process_forms.html)). В **КНД** в файле  свойств проекта определено:
```xml
      bpm.formsConfig = bpm-forms.xml officework-bpm-forms.xml
```
То есть помимо стандартной формы определена `E:\KND\knd\components\officework\modules\web\officework-bpm-forms.xml`:
```xml
<?xml version="1.0" encoding="UTF-8"?>  
<forms xmlns="http://schemas.haulmont.com/cuba/bpm-forms.xsd">  
 <form name="rolesProcForm" default="true">  
 <param name="commentRequired" value="true"/>  
 <param name="procActorsVisible" value="true"/>  
 <param name="attachmentsVisible" value="true"/>  
 </form></forms>
```
Таким образом в качестве стандартной формы вызываемой по умолчанию (`default="true"`) при запуске процесса является форма `rolesProcForm`. Класс `com.haulmont.kndofficework.web.bpm.procform.RolesProcForm`  переопределяет стандартную форму `StandardProcForm`, Так же переопределен и стандартный фрейм отображающий процессные роли  `com.haulmont.kndofficework.web.bpm.frame.procactor.ProcActorFrameExt`где добавлена возможность выбирать и сопоставлять процессным ролям реальных пользователей системы.
В экране `планирование КНД` (`check-plan-combined.xml`)  определен:
```xml
<frame id="planActionsFrame" screen="procActionsFrame" width="AUTO" visible="false"/>
```
а в контролере экрана инжектирован опять же переопределенный процессный фрейм `com.haulmont.kndofficework.web.bpm.frame.procactor.ProcActorFrameExt`:
```java
@Inject  
private ProcActionsFrameExt planActionsFrame;
``` 
Для отображения в процессном фрейме действия соответствующего текущей процессной роли пользователя он должен быть инициализирован. Такая инициализация происходит в методе `init()` контролера экрана `CheckPlanCombined` при выборе элемента плана в `checkPlansDs.addItemChangeListener` выполняется процедура `refreshProcessFrame(e.getItem());`
```java
protected void refreshProcessFrame(CheckPlan checkPlan) {  
    if (checkPlan == null || checkPlan.getEventType() == null || checkPlan.getEventType().getProcDefinitionPlan() == null)  
        return;  
  planActionsFrame.initializer()  
            .setAfterStartProcessListener(this::afterStartProcess)  
            .setAfterCompleteTaskListener(this::afterStartProcess)  
            .setAfterClaimTaskListener(this::afterStartProcess)  
            .setAfterCancelProcessListener(this::afterStartProcess)  
            .init(checkPlan.getEventType().getProcDefinitionPlan().getCode(), checkPlan);  
}
```
Определение процесса зависит от вида проверки `EventType` - в таблице `knd_event_type` указана ссылка `proc_definition_plan_id` на таблицу описанием процесса `bpm_proc_definition` которое получается в результате развертывания соответствующей модели.
Таким образом для каждого пользователя определены процессные действия которые он может совершить исходя из своей роли в системе и процессной роли для конкретного экземпляра сущности `checkPlan`.
При инициализации процессного фрейма может быть организована проверка выполнения тех или иных условий при прохождении этапов процесса. Например в экране проверок `CheckHoldingScreen`   в процедуре `initProcActionsFrame(reloadedItem.getEventKind().getProcDefinitionHolding().getCode());` процессный фрейм инициализирован следующим образом:
```java
private void initProcActionsFrame(String processCode) {  
    procActionsFrame.initializer()  
            .setBeforeCompleteTaskPredicate(this::beforeComplete)  
            .setAfterStartProcessListener(checksDs::refresh)  
            .setAfterCompleteTaskListener(checksDs::refresh)  
            .init(processCode, checkDs.getItem());  
}
```
При инициализации определен предикат `beforeComplete` котором проверяется наличие необходимых документов для данного этапа процесса:
```java
private boolean beforeComplete() {  
    return documentProcessHelper.checkRequiredDocuments(checksDs.getItem());  
}
```


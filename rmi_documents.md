# Реализация документооборота в модуле РМИ

## Модель аспектов
### Базовые свойства документов
```xml
        <aspect name="rmi:doc">
           <title>RMIDOC</title>
           <properties>
               <!-- Наименование типа документа -->
                <property name="rmi:clientId">
                   <type>d:long</type>
				   <mandatory>true</mandatory>  
               </property>
			   <!-- Тип документа -->
               <property name="rmi:CategID"> 
                   <type>d:long</type>
				   <mandatory>true</mandatory>  
               </property>
               <!-- Номер документа -->
                <property name="rmi:RegNumber">
                   <type>d:text</type>
				   <mandatory>true</mandatory>  
               </property>
               <!-- Дата документа -->
                <property name="rmi:RegDate">
                   <type>d:date</type>
				   <mandatory>true</mandatory>  
               </property>
               <!-- Содержание -->
                <property name="rmi:Contents">
                   <type>d:text</type>
               </property>
               <!-- Кто утвердил документ -->
                <property name="rmi:GrantorID">
                   <type>d:long</type>
               </property>
               <!-- Дата утверждения документа -->
                <property name="rmi:GrantDate">
                   <type>d:date</type>
               </property>
               <!-- Кому выдан документ -->
                <property name="rmi:GranterID">
                   <type>d:long</type>
               </property>
               <!-- Когда выдан документ -->
                <property name="rmi:GranterDate">
                   <type>d:date</type>
               </property>
               <!-- Сколько копий выдано -->
                <property name="rmi:Quantity">
                   <type>d:int</type>
               </property>
               <!-- Примечание -->
                <property name="rmi:Comments">
                   <type>d:text</type>
               </property>
               <!-- Дата начала действия документа -->
                <property name="rmi:StartDate">
                   <type>d:date</type>
               </property>
               <!-- Дата окончания действия документа -->
                <property name="rmi:EndDate">
                   <type>d:date</type>
               </property>
               <!-- Строка поиска -->
                <property name="rmi:allSearch">
                   <type>d:text</type>
               </property>
           </properties>
       </aspect>
```
### Связанные документы
```xml
	   <aspect name="rmi:addDocs">
            <title>ADDDOCS</title>
           <properties>
			   <property name="rmi:parentDocs">
                   <type>d:text</type>
                    <multiple>true</multiple>
               </property>
			   <property name="rmi:childDocs">
                   <type>d:text</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
```
**Здания (по аналогии другие типы объектов)**
```xml
       <!-- Документы привязанные непосредственно к зданию -->   
       <aspect name="rmi:buildDoc">
            <title>BUILDDOCS</title>
           <properties>
			   <property name="rmi:buildIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к записи о правообладателе здания -->   
       <aspect name="rmi:buildOwnDoc">
            <title>BUILDOWNERDOCS</title>
           <properties>
		       <!-- Наступление права --> 
			   <property name="rmi:buildOwnStartIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
		       <!-- Окончание права --> 
			   <property name="rmi:buildOwnEndIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к записи о балансодержателе здания -->   
       <aspect name="rmi:buildBalanceDoc">
            <title>BUILDBALANCEDOCS</title>
           <properties>
		       <!-- Передача на баланс -->
			   <property name="rmi:buildBalanceStartIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
		       <!-- Снятие с  баланса -->
			   <property name="rmi:buildBalanceEndIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к записи о Сделке/Договоре/Контракте здания -->   
       <aspect name="rmi:buildContractDoc">
            <title>BUILDCONTRACTDOCS</title>
           <properties>
			   <property name="rmi:buildContractIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к адресной записи здания -->   
	   <aspect name="rmi:buildAddrDoc">
            <title>BUILDADDRDOCS</title>
           <properties>
			   <property name="rmi:buildCLAddressIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к ОКН -->   
	   <aspect name="rmi:buildOKNDoc">
            <title>BUILDOKNDOCS</title>
           <properties>
			   <property name="rmi:buildOKNIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к статусу здания -->   
	   <aspect name="rmi:buildStatusDoc">
            <title>BUILDSTATUSDOCS</title>
           <properties>
			   <property name="rmi:buildStatusIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
       <!-- Документы привязанные к ограничениям здания -->   
	   <aspect name="rmi:buildBurdenDoc">
            <title>BUILDBURDENDOCS</title>
           <properties>
				<property name="rmi:bBBurdensIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
                </property>
  			    <property name="rmi:bCBurdensIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
                </property>
           </properties>
      </aspect>
```
### Реестр
```xml
	  <aspect name="rmi:reestrDoc">
            <title>REESTRDOCS</title>
           <properties>
			   <property name="rmi:reestrIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
```
### ФЛ/ЮЛ
```xml
	   <aspect name="rmi:OwnersDoc">
            <title>OWNERSDOCS</title>
           <properties>
			   <property name="rmi:OwnFirmsIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
			   <property name="rmi:OwnPersonsIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
```
### Балансодержатели
```xml
	   <aspect name="rmi:BalanceOwnerDoc">
            <title>BALANCEOWNERSDOCS</title>
           <properties>
			   <property name="rmi:BalanceOwnerIDs">
                   <type>d:long</type>
                    <multiple>true</multiple>
               </property>
           </properties>
       </aspect>
```       
## Таблица соответствия аспектов сущностям РМИ
Для проверки связей документов с сущностями РМИ реализована сущность `cubaobj$DocAspects`. Таблица связей выглядит следующим образом (на примере Зданий):
| aspect               | property                 | entity_name           | parent_entity |
|----------------------|--------------------------|-----------------------|---------------|
| rmi:buildContractDoc | rmi:buildContractIDs     | BuildingContracts     | Buildings     |
| rmi:buildBurdenDoc   | rmi:bCBurdensIDs         | BuildingContracts     | Buildings     |
| rmi:buildAddrDoc     | rmi:buildCLAddressIDs    | BuildingAddress       | Buildings     |
| rmi:buildOKNDoc      | rmi:buildOKNIDs          | Buildings             | Buildings     |
| rmi:buildBalanceDoc  | rmi:buildBalanceEndIDs   | BuildingBalanceOwners | Buildings     |
| rmi:buildBalanceDoc  | rmi:buildBalanceStartIDs | BuildingBalanceOwners | Buildings     |
| rmi:buildOwnDoc      | rmi:buildOwnEndIDs       | BuildingOwners        | Buildings     |
| rmi:buildOwnDoc      | rmi:buildOwnStartIDs     | BuildingOwners        | Buildings     |
| rmi:buildDoc         | rmi:buildIDs             | Buildings             | Buildings     |
| rmi:buildStatusDoc   | rmi:buildStatusIDs       | BuildingStatuses      | Buildings     |
| rmi:buildBurdenDoc   | rmi:bBBurdensIDs         | BuildingBalanceOwners | Buildings     |


## Код обеспечивающий ссылочную целостность для виртуального DataSet документов

### Базовые свойства документов
Получения виртуального `DataSource` происходит через класс `DocumentsDatasource` получающий коллекцию Документов через службу среднего уровня `GetDocsService` (интерфейс `GetDocsService`, бин `GetDocsServiceBean`). 
Логика операций создания, изменения, удаления сущности `cubaobj$Documents` реализована в контролере экрана `DocumentsBrowse`. Здесь же  реализована функция проверяющая связь документа с любым аспектом системы при удалении `documentBindedWithAnyObject` через сущность связку `cubaobj$DocAspects`.
Логика операций по добавлению удалению прикрепленных файлов, связыванию (удалению связи) родительских и дочерних документов реализована в контроллере `DocumentsEdit`. Так же в этом контролере в методе `preCommit()` реализована проверка наличия одинаковых документов по номеру, дате и виду документа и функция проверки не был ли уже связаны два документа ранее `documentBindedWithDoc`.
Для базовых свойств документа обязательной является ссылка на персистентный иерархический справочник видов документов `cubadicts$DictDocsCategories`. Поэтому при  удалении экземпляра данного справочника производится проверка на его связь с документами  поиском по значению свойства аспекта. Код проверки помещен в `BeforeActionPerformedHandler` для `Action.Remove` в браузере справочника  `DictDocsCategoriesBrowse`:
```java
dictDocsCategoriesesTableRemove.setBeforeActionPerformedHandler(() -> {  
    List<String> findFolders = new ArrayList<>();  
  DictDocsCategories curDict = dictDocsCategoriesesTable.getSingleSelected();  
 boolean find = false;  
  List<SearchAspectDTO> aspects = new ArrayList<>();  
  aspects = CmisSearchParamsBuilder.buildSearchAspectList()  
            .addSearchAspect(b -> b.setAspectName("rmi:doc")  
                    .addSearchProperty(p -> p.setPropertyName("rmi:CategID").setValue("'" + String.valueOf(curDict.getId()) + "'"))  
            ).build();  
  findFolders = cmisAPI.getService().getFolderNamesByAspects(aspects);  
  find= findFolders.size()>0 ? true: false;  
 if (find) {  
        showNotification("Данный элемент справочника связан с другими объектами системы и не может быть удален!", NotificationType.ERROR);  
 return false;  } else {  
        return true;  
  }  
});
```
### Работа с документами в сущностях (на примере зданий)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY3NTg5MDQxNiw0MTc5MTUxMDIsLTEzNj
UyNTEzMzEsLTQ5ODMyMTI4Niw0NTk1NTI5NDUsLTc4NDczNzcz
NSwtMzYzNTM2OTUsNjc2NjAwNjMzLC0yMDg4NzQ2NjEyXX0=
-->
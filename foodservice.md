## Проект FoodService
### Полезные ссылки
1. Установка NodeJs Уебунту
   https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru
2. Команды терминала Уебунту
   https://help.ubuntu.ru/manual/терминал   
3. Стягивание файла через putty
     pscp.exe root@95.216.203.238:/root/archive.zip "E:\Илья"
4. Архивирование Уебунту
 >zip -r archive.zip <directory_name>
  https://linux-freebsd.ru/linux/programmnoe_obespechenie-linux/arhivirovanie-s-pomoshhyu-zip-v-debian-ubuntu-linux-arhivator-zip/
5. Простой бэкэнд 
http://cocoa-beans.ru/backend/simple-node-backend/     
6. Работа с PG
https://node-postgres.com
7. Пример full stack с NodeJs и Posgres
https://scotch.io/tutorials/getting-started-with-node-express-and-postgres-using-sequelize
8. Дока по sequelize
http://docs.sequelizejs.com

### Развертывание проекта
Конфиги постгреса
>  /etc/postgresql/10/main

Перезапуск  постгрес
>sudo service postgresql restart

Удаление папки (файла -- без параметров)
>rm -Rfv foodservice

Создание папки
>mkdir foodservice

Переход в паку
>cd foodservice

Инициализация (создание `package.json`)
>npm init <-y>

Старт проекта
>$ npm run start:dev

Установка зависимостей
>npm install --save --exact express body-parser pg
>

Установка MongoDB
https://timeweb.com/ru/community/articles/kak-ustanovit-mongodb-na-ubuntu-18-04-1

Наше все
https://www.youtube.com/watch?v=56TizEw2LgI&list=PL55RiY5tL51rajp7Xr_zk-fCFtzdlGKUp

NodeJs
https://www.youtube.com/playlist?list=PL55RiY5tL51oGJorjEgl6NVeDbx_fO5jR
     
### Развертывание проекта для разработки
установка NodeJs
```bash
sudo apt install nodejs  
sudo apt install npm  
npm install --save express
```
установка Express generator (не нужно если уже есть проект)
```bash
sudo apt install node-express-generator
```
```bash
sudo apt install -y mongodb
```
Инсталляция mongoose (не нужно так как в проекте будем делать команду ниже)
```bash
npm install mongoose 
```
В папке проекта выполняем (устанавливает все зависимости)
```bash
npm install
```
Запуск 
```bash
npm start
```
### Развертывание проекта на удаленном сервере
Клонируем проект из репозитория:
```bash
git clone https://gitlab.com/Postolov/foodservice.git
``` 
Создаем службу из описания в папке deploy
https://www.axllent.org/docs/view/nodejs-service-with-systemd/
https://nodesource.com/blog/running-your-node-js-app-with-systemd-part-1/
```bash
sudo nano /lib/systemd/system/foodserver.service
```
выдаем права доступа скрипту
```bash
chmod +rwx runfoodserver.sh
```
запускаем службу
```bash
sudo systemctl start foodserver
sudo systemctl stop foodserver
```
просмотр системного журнала
```bash
sudo tail /var/log/syslog
```



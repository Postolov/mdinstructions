## KND tips
**Обновление из базового репо knd_gp**
из папки с проектом
```bash
git pull http://78.46.65.19:9090/knd/kndgp
```
**Загрузка дампа .sql средствами psql**

В случае если локально установлен PostgreSql сервер путь к psql добавлен в переменную среды PATH
Иначе psql можно найти по пути `C:\Users\{USERNAME}\.dbeaver-drivers\clients\postgresql\win`
Для удаленного сервера
```bash
psql --host=95.216.203.238 --dbname=knd --username=root --file="E:\KND\knd.sql"
```
Для локального сервера
```bash
psql --dbname=knd --username=root --file="E:\KND\knd.sql"
```

**Update DB**
```bash
gradlew updateDb -PdbHost=95.216.203.238 -PdbUser=root -PdbPass=root
gradlew updateDb -PdbHost=78.46.65.19 -PdbName=knd_technadzor -PdbUser=postgres -PdbPass=b6VA4hKVpTqVqFfR
gradlew updateDb -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_tehnadzor
gradlew updateDb -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_chsnadzor
gradlew updateDb -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_kultnadzor
```
**Сборка приложения:**
сначала (один раз после клонирования репо) :
```bash
git submodule init
git submodule update
```
потом
```bash
gradlew -b build-bootstrapper.gradle buildApplication
```
**Запуск приложения с нашей базой**
```bash
gradlew start -PdbHost=95.216.203.238 -PdbUser=root -PdbPass=root
gradlew start -PdbHost=78.46.65.19 -PdbUser=root -PdbPass=root
```
**Запуск приложения с локальной БД**
```bash
gradlew start -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_lekarstvanadzor
gradlew start -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_chsnadzor
gradlew start -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_tehnadzor
gradlew start -PdbHost=localhost -PdbUser=root -PdbPass=root -PdbName=knd_kultnadzor
```

**Останов приложения**
```bash
gradlew stop
```
**Регламент**
https://docs.google.com/document/d/1hM--TSSVo3NgrOkoZ_pw3dIIdZfZ3ISWCBUUJa7Q65c/edit#heading=h.qajttobzq8re

**Ограничения в выборке в DataSource**
https://doc.cuba-platform.com/manual-6.10-ru/datasource_query_params.html

**Конфигурационные интерфейсы **
В модуле ``global`` создаем пакет ``configuration`` и там соответствующий интерфейс:
```java
package com.gispuzzle.kndgp.configuration;  
  
import com.gispuzzle.kndgp.entity.KultObjectType;  
import com.haulmont.cuba.core.config.Config;  
import com.haulmont.cuba.core.config.Property;  
import com.haulmont.cuba.core.config.Source;  
import com.haulmont.cuba.core.config.SourceType;  
  
@Source(type = SourceType.DATABASE)  
public interface KNDGPApplicationConfiguration extends Config {  
  
    @Property("kndgp.configuration.kultObjectType")  
    KultObjectType getDefaultKultObjectType();  
}
```
при этом самостоятельно в базе в таблице ``sys_config`` создается соответствующее свойство, после чего оно доступно из меню _Администрирование->Свойства приложения_
далее его можно использовать для установки начальных значений

**Установка начальных значений**
Вариант 1 - запросом
```java
@Inject  
DataManager dataManager;  
@Override  
protected void initNewItem(SupervisedObject item) {  
	((KultExtendedSupervisedObject)item).
	setKultSupervisedObjectType(dataManager.load(LoadContext.create(KultObjectType.class).  
	setQuery(LoadContext. createQuery("select e from kndgp$KultObjectType e where e.kultObjectTypeName LIKE CONCAT('%',:str,'%')").
	setParameter("str", "Выявленный"))));
}  
```
Вариант 2 - через умолчальное значение конфигурации
```java
public class SupervisedObjectEditExt extends SupervisedObjectEdit {  
	@Inject  
	protected KNDGPApplicationConfiguration cfg;
	@Override  
	  protected void initNewItem(SupervisedObject item) {  
	 ((KultExtendedSupervisedObject) item).setKultSupervisedObjectType(cfg.getDefaultKultObjectType());  
     }  
}
```


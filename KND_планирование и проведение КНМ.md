## Процессы планирования и проведения КНМ (v0.10)

### Плановые проверки, Программа проверок
Модель процесса "Согласование плана проверок"

![enter image description here](https://lh3.googleusercontent.com/pkbn_gJ98wkBMtz5Q0xYQPPD0NcpzavDsVQ18U91fQlo0B25Iq001b3tKkkvpr260jWIC-82gaTv=s1200)

Процессные роли:

![enter image description here](https://lh3.googleusercontent.com/JGQykBCPS1zXAH5WqMxRzBWcMuYKBMeMAj_yku4BqWsBKcGPJ2dPVJpBsbCUga-x21KxnQnxwvKs=s800)

После создания плана проверок и ввода имени  плану присваивается статус `Редактируется` и процессному фрейму назначается выход `Отправить`. Процессная роль `sender`. 

Старт процесса происходит автоматически. В слушателе событий жизненного цикла экземпляров сущностей плана проверки на уровне Middleware  `com.haulmont.knd.core.entity.listeners.CheckPlanListener` публикуется событие:

```java
@Override  
public void onAfterInsert(CheckPlan entity, Connection connection) {  
    publisher.publishEvent(new CheckPlanEvent(entity));  
}
```
Слушатель событий сущности `com.haulmont.knd.core.event.listener.CheckPlanEventListener` обрабатывает событие соответствующее созданию экземпляра плана и запускает нужный процесс:

```java
@Transactional(propagation = Propagation.REQUIRES_NEW)  
@TransactionalEventListener(condition = "!#event.persisted")  
public void onPlanCreated(CheckPlanEvent event) {  
    CheckPlan checkPlan = event.getCheckPlan();  
  ProcDefinition procDefinition = checkPlan.getEventType().getProcDefinitionPlan();  
 if (procDefinition != null) {  
        processHelper.startProcess(procDefinition, checkPlan);  
  }  
}
```
Определение процесса зависит от типа проверки `EventType` - в таблице `knd_event_type` указана ссылка `proc_definition_plan_id` на таблицу описанием процесса `bpm_proc_definition` которое получается в результате развертывания соответствующей модели.

При нажатии на кнопку `Отправить` появляется стандартное процессное окно (форма):

![enter image description here](https://lh3.googleusercontent.com/FB91A0XVltXtyJwt6tAHLuctVErqVomgkeU1kDZbM4mtqB9m0cEcv5lKmJ79Nx2x2tIY8xx1qneG=s900)

После отправки плана проверок статус меняется - `На согласовании` (отрабатывает script task):
```groovy
import com.haulmont.knd.entity.CheckPlan
import com.haulmont.knd.entity.PlanStatus

def em = persistence.getEntityManager()
def plan = em.find(CheckPlan.class, entityId)
plan.setStatus(PlanStatus.ON_AGREEMENT)
```
Процесс переходит к пользовательской задаче `Согласование` которая имеет два выхода: `Согласовать` и  `Доработать`. Процессная роль `verifier`.

>Вопросы
>
> Выходы задачи и переход названы по разному: выход `Доработать` а переход `Отклонить`
> 
> Не происходит проверка наличия субъектов плана, установленного периода и т.п. - процесс переходит на этап согласования без каких либо проверок

При выборе выхода `Доработать` появляется базовое окно (включен только фрейм с комментариями) настройки в описании выходов задачи `Согласование`:

![enter image description here](https://lh3.googleusercontent.com/Trrh3MJIlj651csISqIP0N-IUvp-X5x3kUvHWKE6aDOGUuMEYE4VRs6HMPsaSW59I4xEScCsQuvB=s600)

После отрабатывает script task `Установить статус плана "На доработке"` и скрипт:
```groovy
import com.haulmont.knd.entity.CheckPlan
import com.haulmont.knd.entity.PlanStatus

def em = persistence.getEntityManager()
def plan = em.find(CheckPlan.class, entityId)
plan.setStatus(PlanStatus.CREATED)
```
устанавливает для плана проверок статус `Редактируется`

>не соответствие названия задачи установленному статусу

После чего процесс переходит к задаче `Доработка плана`. Процессная роль `sender` . Задача имеет один выход `Отправить`, в процессной форме отключены все фреймы кроме комментариев (т.е. передать задачу другим исполнителям после доработки нельзя)

>нельзя передать задачу другим исполнителям после доработки

При выборе выхода `Согласовать` script task со скриптом:
```groovy
import com.haulmont.knd.entity.CheckPlan
import com.haulmont.knd.entity.PlanStatus

def em = persistence.getEntityManager()
def plan = em.find(CheckPlan.class, entityId)
plan.setStatus(PlanStatus.ON_APPROVAL)
```
устанавливает для процесса статус `На утверждении` и далее пользователь с процессной ролью `approver` имеет возможность утвердить план проверок с выполнением скрипта:
```groovy
import com.haulmont.knd.entity.CheckPlan
import com.haulmont.knd.entity.PlanStatus

def em = persistence.getEntityManager()
def plan = em.find(CheckPlan.class, entityId)
plan.setStatus(PlanStatus.APPROVED)
```

>почему нет возможности `Не утвердить` план если министра что то не устраивает

Фрейм `Документы` кнопка `Добавить` даже если и активна на некоторых этапах согласования плана, на протяжении всего процесса не позволяет добавить документы в случае `Плановой проверки`.

**События происходящие при завершении процесса "Согласование плана проверок"**

После утверждения плана проверок происходит автоматически сразу несколько действий:

**Во-первых** заполняется значение поля `approvedDate` класса `CheckPlan`. Заполнение этого поля происходит в слушателе событий жизненного цикла экземпляров сущностей на уровне Middleware (`com.haulmont.knd.core.entity.listeners.ProcInstanceListener`)
```java
public class ProcInstanceListener implements BeforeUpdateEntityListener<ProcInstance> {  
  @Inject  
  private PersistenceTools persistenceTools;  
  @Override  
  public void onBeforeUpdate(ProcInstance entity, EntityManager entityManager) {  
        if (persistenceTools.isDirty(entity, "endDate")) {  
            CheckPlan checkPlan = entityManager.find(CheckPlan.class, entity.getEntity().getEntityId());  
 if (checkPlan != null) {  
                checkPlan.setApprovedDate(entity.getEndDate());  
  }  
        }  
    }  
}
```
Поле `approvedDate` заполняется значением поля `endDate` из экземпляра процесса `ProcInstance`. Значение `endDate` соответствует времени окончания процесса и проставляется слушателем описанном в модели процесса:

![enter image description here](https://lh3.googleusercontent.com/DqaSCqziwVtMxQTpxsYSRuhGBq-hVUJMc3jQXzIyQOp1H484gUuxEaRypf1GY7HdwZF-ttxAWX5Q=s900) 

Этот слушатель присутствует по умолчанию во всех моделях и отвечает за создание и изменение сущностей подсистемы BPM при наступлении определенных событий процесса - подробнее [здесь](https://doc.cuba-platform.com/bpm-6.10-ru/bpm_activiti_listener.html).

**Во-вторых** происходит создание процессов соответствующих сущностям субъектов плана. 
В слушателе событий жизненного цикла экземпляров сущностей плана проверки на уровне Middleware  `com.haulmont.knd.core.entity.listeners.CheckPlanListener` публикуется событие:
```java
@Override  
public void onAfterUpdate(CheckPlan entity, Connection connection) {  
    publisher.publishEvent(new CheckPlanEvent(entity));  
}
```
Слушатель событий сущности `com.haulmont.knd.core.event.listener.CheckPlanEventListener` обрабатывает событие соответствующее изменению экземпляра плана и запускает нужный процесс для каждого экземпляра субъекта плана:
```java
@Transactional(propagation = Propagation.REQUIRES_NEW)  
@TransactionalEventListener(condition = "#event.dirtyFields.contains('approvedDate')")  
public void onPlanApprove(CheckPlanEvent event) {  
for (Check check : event.getCheckPlan().getChecks()) {  
        ProcDefinition procDefinition = check.getEventKind().getProcDefinitionHolding();  
        if (procDefinition != null) {  
            processHelper.startProcess(procDefinition, check);  
        }  
    }  
}
```
Определение процесса зависит от вида проверки `EventKind` - в таблице `knd_event_kind` указана ссылка `proc_definition_holding_id` на таблицу описанием процесса `bpm_proc_definition` которое получается в результате развертывания соответствующей модели.

В настоящий момент (v0.10) для видов плана `Плановые проверки, Программа проверок` указан процесс `Плановая проверка`


### Добавление субъектов плана
В зависимости от выбранного типа проверки инициализируется кнопка создания субъектов плана. Инициализация происходит в `com.haulmont.knd.web.checkplan.frame.EventAbstractListFrame`. Данные по возможным вариантам хранятся в `knd_event_kind`.
Окно добавления:

![enter image description here](https://lh3.googleusercontent.com/js9VxPf2De7lzcVWBXlSD1C4rJ3WgHlc12XGMvfnJSznXgQBXzm6-yWNPWtjZNZ4Qkml1jdKo8Dh=s900)

Номер не вводится
Планируемая дата не вводится 
Ответственный не вводиться

**Дополнительные данные**

- Перечень положений - данные нормативных актов (№ (только число), дата, уровень (федеральный, региональный) и т.д.);

- Задачи проверки - простое текстовое значение;

- Предметы проверки - простое текстовое значение;

- Мероприятия по надзору - простое текстовое значение;

- Перечень документов - простое текстовое значение;

- Требования для проверки - простое текстовое значение;

- Уполномоченные лица - по сути справочник должностей который связывает физических лиц с пользователями системы и должностями в КНО - (currentUserHasNotPermissionForCheckError = Выбранный Вами сотрудник не может участвовать в проверке. Пожалуйста выберите другого сотрудника. - в случае если `!officerKNO.getHasPermissionsForCheck()` - должны быть привилегии на проведение проверки у сотрудника КНО );

- Правовые основания для проведения проверки -  данные нормативных актов;


> с сотрудниками КНО непонятно - список в меню "Сотрудники КНО" есть - физлиц таких в справочнике нет при попытке выбрать `Уполномоченное лицо` список  сотрудников КНО пуст
>
> при создании субъектов плана никакие проверки не проводятся  можно вставлять в план проверки с одинаковым периодом одного типа по одному и  тому же субъекту

### Планирование. Внеплановые проверки, Профилактические мероприятия, Рейдовые мероприятия.
Модель процесса "Старт внеплановых проверок"
Модель процесса "Старт профилактических мероприятий"
Модель процесса "Старт рейдов"
> в настоящий момент модели процессов `Старт профилактических мероприятий`, `Старт рейдов` не развернуты и для них не созданы экземпляры описания процессов 
> в таблице `knd_event_type` определены ссылки для этих типов планов проверок на описание процесса "Старт внеплановых проверок" 

![enter image description here](https://lh3.googleusercontent.com/-u7wRmFTjPxr59GWMmMaeM9NGGR10ctyb7GGy488DtZxVdTAJS4_Wyp2KOvOzkFrI1crZQ9oFOuA=s900)

Одна процессная роль `sender - Специалист`.
При заведении субъекта плана мероприятий обязательно необходимо указывать ответственного сотрудника. 
> При этом изменить ответственного сотрудника нельзя

Процессный выход `Начать работу` приводит к тому что скриптом производиться изменение стадии планирования на `Утвержден` (в начале процесса для плана установлена стадия `Редактируется`).

После утверждения плана происходят события описанные для типов плана `Плановые проверки, Программа проверок` с той лишь разницей что для описываемых типов плана в таблице `knd_event_kind` указаны ссылки на процесс `Внеплановая проверка` 

Фрейм `Документы` не активен.

> Для этих типов плана вполне можно добавлять субъекты когда план имеет статус `Утвержден` причем для этих случаев даже прописано создание процессов для сущностей субъектов плана в слушателе `com.haulmont.knd.core.entity.listeners.CheckPlanListener`
```java
 @Override  
public void onBeforeInsert(Check entity, EntityManager entityManager) {  
    entity.setNum(uniqueNumbersAPI.getNextNumber(KND_CHECK_NUMBER_SEQUENCE));  
  
  setCheckPlanPeriod(entity, entityManager);  
  
 if (entity.getCheckPlan().getApprovedDate() != null) {  
        ProcDefinition procDefinition = entity.getEventKind().getProcDefinitionHolding();  
 if (procDefinition != null) {  
            processHelper.startProcess(procDefinition, entity);  
  }  
    }  
}
```

>Из-за того что в настоящий момент типам плана `Внеплановые проверки, Профилактические мероприятия, Рейдовые мероприятия` сопоставлен процесс `Внеплановая проверка` то дальнейшее движение идет по этому процессу. В интерфейсе `Моя деятельность` соответственно `Профилактические мероприятия` отображаются как `внеплановая проверка`

>Несмотря на назначение ответственного специалиста при заведении субъектов плана - все мероприятия плана доступны пользователям с процессной ролью `sender` которая единственная определена в модели процесса `Внеплановая проверка`

### Фреймы субъектов плана (EventAbstractListFrame)
Фреймы субъектов плана определены как список наследников от `com.haulmont.knd.web.checkplan.frame.EventAbstractListFrame`. Процессный фрейм  соотвествующий экземплярам субъектов плана изначально невидим для пользователя:
```xml
<hbox align="MIDDLE_RIGHT" spacing="true" height="25px">  
 <frame id="eventActionsFrame" screen="procActionsFrame" width="AUTO" visible="false"/>  
 <popupButton id="eventActionsPB" caption="msg://checkProcActionsPB" width="150px" visible="false"/>  
 <button action="checksTable.download"/>  
 <button action="checksTable.print"/>  
</hbox>
```
Инициализация фрейма происходит после утверждения плана 
```java
protected void refreshProcessFrame(Check selected) {  
    if (selected == null || selected.getEventKind() == null || selected.getEventKind().getProcDefinitionHolding() == null) {  
        eventActionsFrame.reset();  
  eventActionsFrame.setVisible(false);  
  } else {  
        eventActionsFrame.setVisible(true);  
  CheckPlan selectedCheckPlan = checkPlansDs.getItem();  
 boolean enabled = selectedCheckPlan != null  
  && selectedCheckPlan.getApprovedDate() != null  
  && selected != null  
  && selected.getFactDate() == null;  
  eventActionsFrame.setEnabled(enabled);  
  eventActionsFrame.initializer()  
                .setBeforeCompleteTaskPredicate(this::beforeComplete)  
                .setAfterStartProcessListener(this::afterStartProcess)  
                .setAfterCompleteTaskListener(this::afterStartProcess)  
                .setAfterClaimTaskListener(this::afterStartProcess)  
                .setAfterCancelProcessListener(this::afterStartProcess)  
                .init(selected.getEventKind().getProcDefinitionHolding().getCode(), selected);  
  }  
}
```
Следует обратить внимание на то что для процессного фрейма установлен предикат, который проверяет наличие соответствующих документов при движении процесса:
```java
protected boolean beforeComplete() {  
    return documentProcessHelper.checkRequiredDocuments(getCollectionDs().getItem());  
}
``` 
### Плановые проверки (Внеплановые проверки)
Модель процесса "Плановая проверка".
Модель процесса "Внеплановая проверка". (идентична)

![enter image description here](https://lh3.googleusercontent.com/MFlbN1uFtzc42oC9M2hDcVHA04CRrFPgT93-tafDd8s-uvvMn0JRSR40tLbquufS1M_NZpppwXUe=s900)

В модели определена единственная процессная роль `sender`.
Сформированные (запущенные) после утверждения плана проверок процессы доступны пользователям с процессной ролью `sender` в интерфейсе `Моя деятельность` (`com.haulmont.knd.web.myactivity.MyActivityScreen`)  где определен соответствующий процессный фрейм:
```java
private void initProcTaskActionsFrame(ProcTask reloadedItem) {  
  
    switch (reloadedItem.getProcInstance().getEntityName()) {  
        case "knd$CheckPlan":  
            CheckPlan checkPlan = processHelper.getProcessReferencedEntity(reloadedItem.getProcInstance(), "checkPlan-edit");  
 if (checkPlan.getEventType().getCheckType() == CheckType.PLANNED  
  && checkPlan.getChecks().isEmpty()) {  
                procActionsFrame.setVisible(false);  
 return;  }  
            break;  
  }  
  
  procActionsFrame.setVisible(true);  
  procActionsFrame.initializer()  
            .setBeforeStartProcessPredicate(() -> {  
                if (PersistenceHelper.isNew(getItem())) {  
                    showNotification(getMessage("saveProcInstance"), NotificationType.WARNING);  
 return false;  } else {  
                    return commit();  
  }  
            })  
            .setAfterStartProcessListener(new MessageAndCloseAfterActionListener(getMessage("processStarted")))  
            .setAfterCancelProcessListener(new MessageAndCloseAfterActionListener(getMessage("processCancelled")))  
            .setAfterCompleteTaskListener(new MessageAndCloseAfterActionListener(getMessage("taskCompleted")))  
            .setAfterClaimTaskListener(new MessageAndCloseAfterActionListener(getMessage("taskClaimed")))  
            .setBeforeCompleteTaskPredicate(new CommitEditorBeforeActionPredicate())  
            .setBeforeClaimTaskPredicate(new CommitEditorBeforeActionPredicate())  
            .setBeforeCancelProcessPredicate(new CommitEditorBeforeActionPredicate())  
            .setButtonWidth("200px")  
            .init(getItem());  
}
```  
В модели определена начальная процессная задача с выходом `Назначить дату`. В выходе определена процессная форма `com.haulmont.knd.web.bpm.form.standard.DefaultCheckProcForm` которая наследуется от стандартной переопределенной для KND `com.haulmont.kndofficework.web.bpm.procform.RolesProcForm`

![enter image description here](https://lh3.googleusercontent.com/u6NMVgyqtpX7eAIGnv4MIEdXscE9J8QL0lL2_aEy44zgpgcqljrZVxSTP3IUU_4EPlCcB5ip7WT3=s900)

В форме задается фактическая дата проверки и комментарий.

> дата никак не проверяется - можно указать дату раньше начала проверки или позже окончания плана - результат будет один

После установки фактической даты проверки отрабатывает script task `Установить стадию проверки "Инициация"`
```groovy
import com.haulmont.knd.entity.Check
import com.haulmont.knd.entity.EventStage

def em = persistence.getEntityManager()
def check = em.find(Check.class, entityId)
check.setCheckStage(EventStage.INITIATION)
```
Стадии проверки описаны в классе ENUM `com.haulmont.knd.entity.EventStage`
```java
PLANNING(0),   //Планирование   
INITIATION(1), //Инициация  
HOLDING(2),    //Проведение
STOPPED(3),    //Приостановлена  
REALISED(4),   //Проведена  
COMPLETED(5);  //Завершена
```
> Фрейм становиться неактивным  в интерфейсе `Планирование КНМ` так как фактическая дата проверки установлена.
```java
 boolean enabled = selectedCheckPlan != null  
  && selectedCheckPlan.getApprovedDate() != null  
  && selected != null  
  && selected.getFactDate() == null;  
```
> Но в интерфейсах `Проведение КНМ` и `Моя деятельность`  этот же фрейм активен так как таких условий там нет

```java
if (reloadedItem.getEventKind() != null && reloadedItem.getEventKind().getProcDefinitionHolding() != null) {  
    initProcActionsFrame(reloadedItem.getEventKind().getProcDefinitionHolding().getCode());  
  procActionsBox.setVisible(true);  
}
```


Сопоставление связанных документов и стадий проверки производится в справочнике `Виды мероприятий` (таблица `knd_event_stage_document_mapping`)

![enter image description here](https://lh3.googleusercontent.com/WrMjvqk3Z9CcTraDkBghu6g1ztcWo5cMDEwzW_RYt-wC4K0WdGDrV7yCnSwpXm9Tovdwdvca0TMP=s400)

![enter image description here](https://lh3.googleusercontent.com/VzjG97at-g7WXrqs9PQNXUS-vSwIFH940wfTJDwbqzsPTyz-JibORs_Vif81CQsJ1Z2cfXKXefTh=s600)

После инициализации проверки она так же становиться доступна и в интерфейсе `Проведение КНМ`. 
В соответствии с моделью следующая задача `Работа с приказом` имеет процессный выход `Приказ подписан` который и доступен в процессном фрейме. Предикатом `CommitEditorBeforeActionPredicate` проверяется наличие прикрепленных к проверке обязательных документов:
```java
protected class CommitEditorBeforeActionPredicate implements ProcAction.BeforeActionPredicate {  
    @Override  
  public boolean evaluate() {  
        Check check = isCurrentEntityCheck();  
 if (check != null) {  
            if (documentProcessHelper.checkRequiredDocuments(check))  
                return commit();  
  
 return false;  } else  
 return commit();  
  }  
}
```
Во фрейме `Документы` становиться доступна кнопка создания `Приказ о проведении проверки`. После создания обязательного документа становиться доступен процессный фрейм процесса документооборота `Визирование документов`
Кроме того script task `Проверить статус приказа` проверяет статус приказа:
```groovy
import com.haulmont.knd.entity.Check 
import com.haulmont.knd.entity.EventStage 
import com.haulmont.bpm.exception.BpmException
import com.haulmont.kndofficework.entity.DocumentStatus 

def em = persistence.getEntityManager() 
def check = em.find(Check.class, entityId) 

def order = check.documents.find { d -> d.category.code == 'order'} 
if (!order || order.status != DocumentStatus.SIGNED) { 
throw new com.haulmont.bpm.exception.BpmException("Приказ не подписан") 
}
```
После того как приказ получает статус `SIGNED("SIGNED");   // Подписан` процесс переходит к следующей задаче `Уведомление поднадзорного субъекта`, процессная роль `sender`, единственный выход `Уведомить субъекта`. Проверка при этом все еще находиться на стадии `INITIATION(1), //Инициация `. При выходе из задачи никакие дополнительные условия не проверяются - вноситься только комментарий - и процесс переходит к задаче `Проверка` с единственным выходом `Начать проверку`.  Выход из задачи дополнительными условиями не проверяется, после её завершения отрабатывает script task `Установить стадию проверки "Проведение"`
```groovy
import com.haulmont.knd.entity.Check
import com.haulmont.knd.entity.EventStage

def em = persistence.getEntityManager()
def check = em.find(Check.class, entityId)
check.setCheckStage(EventStage.HOLDING)
```
и для проверки устанавливается статус `HOLDING(2),    //Проведение`.
На этом этапе как видно из сопоставления документов стадиям проверки могут быть подготовлены три документа: `Предписание`, `Акт проверки`, `Протокол об административном правонарушении`. Однако ни один из них не является обязательным.

 Кнопка `Добавить` фрема `Документы` активна когда для данной стадии проверки есть список документов определенный для вида мероприятия.  Определение списка документов происходит в функции `refreshDocumentTypesForCreation` :
```java
protected void refreshDocumentTypesForCreation(Check check) {  
    popupCreateDocButton.removeAllActions();  
  popupCreateDocButton.setEnabled(false);  
  
 if (checkIsValid(check)) {  
        List<DocumentType> docTypes = checkLoader.loadDocumentTypesForCheck(check);  
  docTypes.forEach(d -> popupCreateDocButton.addAction(initCreateAction(d)));  
  popupCreateDocButton.setEnabled(!docTypes.isEmpty());  
  }  
}
```
Далее процесс переходит к задаче `Завершение проверки` с единственным выходом `Завершить проверку`. Наличие документов не проверяется.
Далее в модели присутствуют два элемента gateway `Опросный лист заполнен?` и `Нужные документы сформированы?` но из них в версии v0.10 (как и из предыдущей задачи `Завершение проверки`) определен только один выход. Видимо в будущем предполагается ветвление процесса. В текущей версии (v0.10) процесс переходит к script task `Установить стадию проверки "Завершена"` :
```groovy
import com.haulmont.knd.entity.Check
import com.haulmont.knd.entity.EventStage

def em = persistence.getEntityManager()
def check = em.find(Check.class, entityId)
check.setCheckStage(EventStage.COMPLETED)
check.setEndDate(new Date())
```
которая устанавливает для проверки статус `COMPLETED(5);  //Завершена` и дату завершения.
При этом в версии v0.10 статусы `STOPPED(3),    //Приостановлена`  и `REALISED(4),   //Проведена` не используются.

### Визирование документов 
Модель процесса "Визирование документов".
Определено три процессных роли:

![enter image description here](https://lh3.googleusercontent.com/qn6yykIuQJOykZpjgZa23cglxqMQDQi7zV2w3yJLESYkb8RsuU4DreL5apu1p-_xWtFMePQ_j7bM=s600)

Процесс зависит от типа документа и определен в таблице `kndofficework_document_type`. В версии (v0.10) для всех типов документов определен один процесс `Визирование документов`.
Определены следующие статусы документа в классе ENUM `DocumentStatus`
```java
CREATED("CREATED"), // Создан
AGREED("AGREED"),   // Согласован
SIGNED("SIGNED");   // Подписан
```
При создании документа в конструкторе `Document` устанавливается статус
```java
protected String status = DocumentStatus.CREATED.getId();
```
Процесс стартует автоматически при создании документа. Старт процесса прописан в слушателе жизненного цикла сущности `Document` - `com.haulmont.kndofficework.core.listeners.DocumentEntityListener`:

```java
public void onBeforeInsert(Document entity, EntityManager entityManager) {  
    if (entity.getDocNumber() == null) {  
        entity.setDocNumber(String.valueOf(uniqueNumbersAPI.getNextNumber(KNDOFFICEWORK_DOCUMENT_NUMBER_SEQUENCE)));  
  }  
    processHelper.startProcess(entity.getDocumentType().getProcDefinition(), entity);  
}
```
 
Процесс начинается с задачи `Подготовка`, процессная роль `sender`, которая имеет единственный выход `На согласование` со стандартной процессной формой с включенными фреймами комментариев и определения участников процесса.
После процесс переходит к задаче `Согласовать`, процессная роль `verifier`, которая имеет два выхода `Доработать` и `Согласовать`. 
`Доработать`  возвращает  документ к предыдущей задаче `Подготовка`.
После выбора выхода `Согласовать` отрабатывает script task `Скрипт согласования`:
```groovy
import com.haulmont.kndofficework.entity.Document;
import com.haulmont.kndofficework.entity.DocumentStatus;
import java.util.Date;

def em = persistence.getEntityManager();
def document= em.find(Document.class, entityId);
document.setStatus(DocumentStatus.AGREED);
```
и документу присваивается статус `AGREED("AGREED"),   // Согласован`
Процесс переходит к задаче `Подписание`, процессная роль `approver`, которая имеет единственный выход `Подписать`.

> опять же у министра нет возможности не подписать приказ

В финале отрабатывает `Скрипт подписания` :
```groovy
import com.haulmont.kndofficework.entity.Document;
import com.haulmont.kndofficework.entity.DocumentStatus;
import java.util.Date;

def em = persistence.getEntityManager();
def document= em.find(Document.class, entityId);
document.setSignDate(new Date());
document.setStatus(DocumentStatus.SIGNED);
```
и устанавливает для документа статус `SIGNED("SIGNED");   // Подписан`. 
Как и в случае с Планом проверок и Проверкой стандартный слушатель `com.haulmont.bpm.core.engine.listener.BpmActivitiListener` устанавливает дату окончания (EndDate) процесса.

### Удаление сущности
При удалении любой сущности с которой связан процесс (План проверок, Проверка, Документ) в соответствующем слушателе жизненного цикла сущности прописано прерывание связанного процесса, например для `CheckPlan` - `com.haulmont.knd.core.entity.listeners.CheckPlanListener`:
```java
@Override  
public void onBeforeDelete(CheckPlan entity, EntityManager entityManager) {  
    Query query = entityManager.createQuery(  
            "select p from bpm$ProcInstance p where p.entity.entityId = :checkPlanId");  
  query.setParameter("checkPlanId", entity.getId());  
  ProcInstance related = (ProcInstance) query.getFirstResult();  
  
 if (related != null) {  
        if (related.getEndDate() == null) {  
            processRuntimeService.cancelProcess(related, "CheckPlan deleted");  
  }  
    }  
}
```

 
